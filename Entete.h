#ifndef __ENTETE_H_
#define __ENTETE_H_

#include <cstdio>
#include <cstdlib> 
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <ctime>  
#include <cmath>
#include <vector>
using namespace std;


struct TProblem							//**D�finition du probl�me:
{
	std::string Nom;					//**Nom du fichier de donn�es
	int Nb;				                        //**Taille du probleme: Nombre de sites/unit�s
	std::vector<std::vector <int> > Flux;		//**Flux entre chaque paire d'unit�s. NB: Tableau de 0 � Nb-1.  Indice 0 utilis�.
	std::vector<std::vector <int> > Distance;	//**Distance entre chaque paire de sites. NB: Tableau de 0 � Nb-1.  Indice 0 utilis�.
};

struct TSolution						//**D�finition d'une solution: 
{
	std::vector<int> Seq;				//**Indique la permutation i.e. le site affect� � l'unit� donn�e en indice. NB: Tableau de 0 � Nb-1.
	long FctObj;						//**Valeur de la fonction obj: Cout total de transit (flux * distance).	
};

struct TAlgo
{
	int		CptEval;					//**COMPTEUR DU NOMBRE DE SOLUTIONS EVALUEES. SERT POUR CRITERE D'ARRET.
	int		NB_EVAL_MAX;				//**CRITERE D'ARRET: MAXIMUM "NB_EVAL_MAX" EVALUATIONS.
};

enum Type
{
	SWAP_RANDOM,
	SWAP_ITERATIVE_FIRST_IMPROVE,
	SWAP_ITERATIVE_BEST_IMPROVE,
	SWAP_PROGRESSIVE
};

#endif