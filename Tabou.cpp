#include <windows.h>
#include "Entete.h"
#include <algorithm>
#include <boost/circular_buffer.hpp>

#pragma comment (lib,"TabouDLL.lib")  

//%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTANT: %%%%%%%%%%%%%%%%%%%%%%%%% 
//Le fichier de probleme (.dat) doit se trouver dans le r�pertoire courant du projet pour une ex�cution � l'aide du compilateur.
//Les fichiers de la DLL (TabouDLL.dll et TabouDLL.lib) doivent se trouver dans le m�me r�pertoire que l'ex�cutable (.exe-DEBUG) et 
//dans le r�pertoire courant du projet pour une ex�cution � l'aide du compilateur.
//Indiquer les arguments du programme dans les propri�t�s du projet - d�bogage - arguements.
//Sinon, utiliser le r�pertoire execution.

//*****************************************************************************************
// Prototype des fonctions se trouvant dans la DLL 
//*****************************************************************************************
//DESCRIPTION:	Lecture du Fichier probleme et initialiation de la structure Problem
extern "C" _declspec(dllimport) void LectureProbleme(std::string FileName, TProblem & unProb, TAlgo &unAlgo);

//DESCRIPTION:	Fonction d'affichage � l'�cran permettant de voir si les donn�es du fichier probl�me ont �t� lues correctement
extern "C" _declspec(dllimport) void AfficherProbleme (TProblem unProb);

//DESCRIPTION: Affichage d'une solution a l'�cran pour validation
extern "C" _declspec(dllimport) void AfficherSolution(const TSolution uneSolution, const TAlgo unAlgo, int N, std::string Titre);

//DESCRIPTION: Affichage � l'�cran de la solution finale, du nombre d'�valuations effectu�es et de certains param�tres
extern "C" _declspec(dllimport) void AfficherResultats (const TSolution uneSol, TProblem unProb, TAlgo unAlgo);

//DESCRIPTION: Affichage dans un fichier(en append) de la solution finale, du nombre d'�valuations effectu�es et de certains param�tres
extern "C" _declspec(dllimport) void AfficherResultatsFichier (const TSolution uneSol, TProblem unProb, TAlgo unAlgo, std::string FileName);

//DESCRIPTION:	�valuation de la fonction objectif d'une solution et MAJ du compteur d'�valuation. 
//				Retourne un long repr�sentant le cout total de transit (flux*distances)
extern "C" _declspec(dllimport) long EvaluerSolution(const TSolution uneSolution, TProblem unProb, TAlgo &unAlgo);

//DESCRIPTION:	Cr�ation d'une s�quence al�atoire (permutation) et �valuation de la fonction objectif. Allocation dynamique de m�moire
// pour la s�quence (.Seq)
extern "C" _declspec(dllimport) void CreerSolutionAleatoire(TSolution & uneSolution, TProblem unProb, TAlgo &unAlgo);

//DESCRIPTION: Copie de la s�quence et de la fonction objectif dans une nouvelle TSolution. La nouvelle TSolution est retourn�e.
extern "C" _declspec(dllimport) void CopierSolution (const TSolution uneSol, TSolution &Copie, TProblem unProb);

//DESCRIPTION:	Lib�ration de la m�moire allou�e dynamiquement
extern "C" _declspec(dllimport) void	LibererMemoireFinPgm	(TSolution uneCourante, TSolution uneNext, TSolution uneBest, TProblem unProb);

// ========================================================================================== //
//                                         PROTOTYPES                                         //
// ========================================================================================== //

//DESCRIPTION: M�thode g�n�rique permettant l'ex�cution de diff�rentes m�thodes de r�solution
void findBestAlternative(TSolution &current, TSolution &best, TProblem instance, TAlgo &algo);

//DESCRIPTION: Ensemble de m�thodes li�es au voisinage
int generateNeighbourhoodSize(const int problemSize);
int generateTabouSizeList(const int problemSize);
vector<TSolution> generateNeighbourhood(TSolution &next, TSolution &best, const TProblem instance, TAlgo &algo, boost::circular_buffer<int> &tabouList);
void addNeighbour(const int hashFromPair, const TSolution &next, vector<TSolution> &kResults, boost::circular_buffer<int> &tabouList);
void addNeighbourAsTabou(const int hashFromPair, const TSolution &next, vector<TSolution> &kResults, boost::circular_buffer<int> &tabouList);

//DESCRIPTION: Ensemble de m�thodes li�es � la selection d'une solution parmi un ensemble
TSolution findBestSolutionFromArray(const vector<TSolution> &kResults);
TSolution pickOneSolutionFromArray(const vector<TSolution> &kResults);
boolean aspirationCriterionIsPositive(const TSolution &next, const TSolution &best);

//DESCRIPTION: Ensemble de m�thodes utilitaires (affichage, �change d'�l�ments dans une liste, ...)
int pickIndexAtRand(const int upperBound);
int pickAvailableIndexAtRandom(const int sourceIndex, const int upperBound);
int getUniqueHashFromPair(const int a, const int b);
void swapElement(TSolution &seq, const int index1, const int index2);
void printEvaluation(const TSolution &current, const TAlgo &algo);
void printString(const std::string message);

// ========================================================================================== //
//                                            MAIN                                            //
// ========================================================================================== //

int main(int NbParam, char *params[])
{
	TSolution initial;  //Solution active au cours des it�rations
	TSolution next;		//Solution voisine retenue � une it�ration
	TSolution best;		//Meilleure solution depuis le d�but de l'algorithme
	TProblem instance;	//D�finition de l'instance de probl�me
	TAlgo algo;			//D�finition des param�tres de l'agorithme
	string filename;
			
	// Lecture des param�tres & du fichier de donnees
	filename.assign(params[1]);
	algo.NB_EVAL_MAX= atoi(params[2]);
	LectureProbleme(filename, instance, algo);
	
	// Cr�ation de la solution initiale 
	CreerSolutionAleatoire(initial, instance, algo);
	best = initial;
	cout << endl << " - Initial solution (randomized) scored " << best.FctObj << endl;

	// Resolution du probl�me en utilisant l'algorithme sp�cifi�
	findBestAlternative(initial, best, instance, algo);

	// Affichage et sauvegarde des r�sultats
	AfficherResultats(best, instance, algo);
	AfficherResultatsFichier(best, instance, algo,"Resultats.txt");
	
	// Nettoyage de la m�moire et arr�t
	LibererMemoireFinPgm(initial, next, best, instance);
	system("PAUSE");
	return 0;
}


void findBestAlternative(TSolution &current, TSolution &best, TProblem instance, TAlgo &algo)
{	
	int rerunCount = 0;
	
	vector<TSolution> kResults;
	boost::circular_buffer<int> tabouList(generateTabouSizeList(best.Seq.size()));

	// Loop until it reaches maxiumum number of evaluations
	while(algo.CptEval < algo.NB_EVAL_MAX) {

		cout << " - Running iteration #" << rerunCount++ << " (evals: " << algo.CptEval << " / " << algo.NB_EVAL_MAX << " => " << algo.NB_EVAL_MAX-algo.CptEval << " left)" << endl;
		kResults = generateNeighbourhood(current, best, instance, algo, tabouList);
		
		current = findBestSolutionFromArray(kResults);

		// Check if there is any improvement
		//	 - [Improvement]: override best result using rerun result
		//   - [No improvement]: simply discard rerun result
		if(current.FctObj < best.FctObj) {
			cout << endl << "	[IMPROVEMENT] Using rerun result as new current best (" << current.FctObj << ")" << endl << endl;
			best = current;
		}
	}
}

// ========================================================================================== //
//                                         NEIBOURHOOD                                        //
// ========================================================================================== //

int generateNeighbourhoodSize(const int problemSize)
{
	return 4*problemSize;
}

int generateTabouSizeList(const int problemSize)
{
	return problemSize;
}

vector<TSolution> generateNeighbourhood(TSolution &next, TSolution &best, const TProblem instance, TAlgo &algo, boost::circular_buffer<int> &tabouList)
{
	int sequenceSize = best.Seq.size();
	vector<TSolution> kResults;
	vector<int> tempPairs;

	unsigned int neighbourhoodSize = generateNeighbourhoodSize(sequenceSize);
	kResults.reserve(neighbourhoodSize);
	tempPairs.reserve(neighbourhoodSize);

	//Loop a random 4 destinations index for each soucre index
	do {
		int sourceIndex = pickIndexAtRand(sequenceSize);
		int indexRand = pickAvailableIndexAtRandom(sourceIndex, sequenceSize);
		int hashFromPair = getUniqueHashFromPair(sourceIndex, indexRand);

		if(find(tempPairs.begin(), tempPairs.end(), hashFromPair) == tempPairs.end()) {
			tempPairs.push_back(hashFromPair);
			swapElement(next,sourceIndex,indexRand);					
			next.FctObj = EvaluerSolution(next,instance,algo);
			
			if(find(tabouList.begin(), tabouList.end(), hashFromPair) == tabouList.end()) {				
				addNeighbourAsTabou(hashFromPair, next, kResults, tabouList);
			} else {
				if(aspirationCriterionIsPositive(next, best)) {
					cout << endl << "	[EVENT] Aspiration criterion enforced tabou (Current scored " << next.FctObj << " while best scored " << best.FctObj << ")" << endl;
					addNeighbour(hashFromPair, next, kResults, tabouList);
					//best = next;
				}
			}
		}
	} while (kResults.size() < neighbourhoodSize);

	tempPairs.clear();
	return kResults;
}

void addNeighbour(const int hashFromPair, const TSolution &next, vector<TSolution> &kResults, boost::circular_buffer<int> &tabouList)
{
	kResults.push_back(next);
}

void addNeighbourAsTabou(const int hashFromPair, const TSolution &next, vector<TSolution> &kResults, boost::circular_buffer<int> &tabouList)
{
	tabouList.push_back(hashFromPair);
	addNeighbour(hashFromPair, next, kResults, tabouList);
}

// ========================================================================================== //
//                                     SOLUTION FROM ARRAY                                    //
// ========================================================================================== //

TSolution findBestSolutionFromArray(const vector<TSolution> &kResults)
{
	TSolution best = kResults[0];
	for(unsigned int i=1; i<kResults.size(); i++) {
		if(best.FctObj > kResults[i].FctObj) {
			best = kResults[i];
		}
	}

	//cout << "	|DEBUG| Out of " << kResults.size() << " elements, best scores " << best.FctObj << endl;
	return best;
}

TSolution pickOneSolutionFromArray(const vector<TSolution> &kResults)
{
	return kResults[rand() % kResults.size()];
}

boolean aspirationCriterionIsPositive(const TSolution &next, const TSolution &best) 
{
	return next.FctObj < best.FctObj;
}

// ========================================================================================== //
//                                            UTILS                                           //
// ========================================================================================== //

void swapElement(TSolution &seq, const int index1, const int index2) 
{
	int tmpValue = seq.Seq[index1];
	seq.Seq[index1] = seq.Seq[index2];
	seq.Seq[index2] = tmpValue;
}

void printEvaluation(const TSolution &current, const TAlgo &algo)
{
	cout << "CptEval: " << algo.CptEval << " Fct Obj Nouvelle Courante: " << current.FctObj << endl;
}

void printString(const std::string message)
{
	cout << endl << message << endl << endl;
}

int pickIndexAtRand(const int upperBound)
{
	return rand() % upperBound;
}

int pickAvailableIndexAtRandom(const int sourceIndex, const int upperBound)
{
	int randomIndex;
	do {
		randomIndex = rand() % upperBound;
	} while (randomIndex == sourceIndex);
	return randomIndex;
}

int getUniqueHashFromPair(const int a, const int b)
{
	if(a > b) {
		return (a+b) * ((a+b+1)/2) + a;
	} else {
		return (b+a) * ((b+a+1)/2) + b;
	}
}